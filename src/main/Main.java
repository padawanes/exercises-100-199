package main;

import java.util.Scanner;

import exercises.*;

/**
 * @author Cristina
 * @version 1.0
 *
 */
public class Main {
	
	public static void main(String[] args){
		program();
		
	}
	
	/**
	 * <h4>menu()</h4><br>
	 * Display menu
	 * 
	 */
	private static void menu(){
		System.out.println("\nWelcome! Choose an exercise: \n");
		System.out.println("\n\tExercise 1: Kaprekar\'s constant\n");
//		System.out.println("\n\t\n"); For more options
	}
	
	/**
	 * <h4>inputN()</h4><br>
	 * inputs an int
	 * @return n
	 */
	private static int inputN(){
		int n; 
		Scanner sc = new Scanner(System.in);
		n=sc.nextInt();	
		sc.close();
		return n;
	}
	
	private static void select(){
		int n = inputN(); int[][] nums; int[] aux = new int[4];
		switch(n){
		case 1:
			n = inputN();
			Kaprekar kaprekar = new Kaprekar (n, null);
			nums = kaprekar.inputNum(n);
			kaprekar.setNums(nums);
			for (int i=0; i<n; i++){
				for(int j=0; j<4; j++){
					aux[j]=nums[i][j];
				}
				kaprekar.kaprekar(aux);
			}
		}
		
	}
	
	private static void program(){
		menu();
		select();
	}

}
