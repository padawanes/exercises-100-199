/**
 * 
 */
package exercises;

import java.util.Scanner;

/**
 * @author Cristina Luna<br>
 * @version 1.0 <br>
 * 
 * Exercise 1. Kaprekar's constant 6174
 * 
 * 
 */
public class Kaprekar {
	private int n;
	private int[][] nums;

	/**
	 * <h4>Constructor </h4><br>
	 * This method builds a Kaprekar object
	 * @param n: number of cases
	 * @param nums: array with n nums
	 * @return new Kaprekar object
	 */
	public Kaprekar(int n, int[][] nums) {
		this.setN(n);
		this.setNums(nums);
	}

	/**
	 * <h4>getNums() </h4><br>
	 * This method gets Nums array from Kaprekar
	 * @return nums[][]
	 */
	public int[][] getNums() {
		return nums;
	}

	/**
	 * <h4>setNums(int[][] nums) </h4><br>
	 * This method sets int[][] as kaprekar.nums
	 * @param nums
	 */
	public void setNums(int[][] nums) {
		this.nums = nums;
	}

	/**
	 * <h4>getN() </h4><br>
	 * This method gets n from Kaprekar
	 * @return n, number of cases
	 */
	public int getN() {
		return n;
	}
	
	/**
	 * <h4>setN(int n) </h4><br>
	 * This method set N number of cases in Kaprekar
	 * @param n: number of cases
	 */
	public void setN(int n) {
		this.n = n;
	}
	
	/**
	 * <h4>isValid(int[] num) </h4><br>
	 * This method checks if the number is correct 
	 * according to the characteristics of exercise
	 * @param num[]: number
	 * @return true or false if number is valid or not
	 */	
	private static boolean isValid(int[] num){
		boolean aux;
		if (num[0]!=num[1]){
			aux=true;
		}
		else if (num[0]!=num[2]){
			aux=true;
		}
		else if (num[0]!=num[3]){
			aux=true;
		}
		else {
			aux=false;
		}
		return aux;
	}
	
	/**
	 * <h4>isNumber(String string) </h4><br>
	 * This method checks if the number is really a number or a text
	 * @param string: string of number
	 * @return true or false if number is a number or not
	 */	
	public static boolean isNumber (String string){
		try {
			Long.parseLong(string);
		} catch (Exception e){
			return false;
		}
		return true;
	}
	
	/**
	 * <h4>numLower(int[] num) </h4><br>
	 * Order array's digits descending
	 * @param num: a number
	 * @return num ordered descending
	 */	
	private static int[] numLower(int[] num){//(>)
		int aux=0, iaux=0; int[] auxNum = new int[4];
		for (int i=0; i<4; i++){
			for (int j=0; j<4; j++){
				if (num[j]>=aux){
					aux = num[j];
					iaux=j;
				}
			}
			num[iaux]=0;
			auxNum[i]=aux;
			aux=0;
		}
		return auxNum;
	}
	
	/**
	 * <h4>numUpper(int[] num) </h4><br>
	 * Order array's digits ascending
	 * @param num: a number
	 * @return num ordered ascending
	 */	
	private static int[] numUpper(int[] num){//(<)
		//TODO correct this method
		int aux; num=numLower(num);
		aux=num[0];
		num[0]=num[3];
		num[3]=aux;
		aux=num[1];
		num[1]=num[2];
		num[2]=aux;
		return num;
	}
	
	
	/**
	 * <h4>subtraction(int[] num1, int[] num2) </h4><br>
	 * subtracts two numbers
	 * @param num1: >
	 * @param num2: <
	 * @return result
	 */	
	private static int[] subtraction(int[] num1, int[] num2){//subtraction
		int subtraction[] = new int[4];
		for (int i=0; i<4; i++){
			subtraction[i]=num1[i]-num2[i];
		}
		return subtraction;
	}
	
	
	/**
	 * <h4>printNum(int[] num) </h4><br>
	 * display a number
	 * @param num
	 */	
	private static void printNum(int[] num){ //Prints a number
		System.out.println(num[0]+num[1]+num[2]+num[3]+"\n");
	}
	
	
	/**
	 * <h4>kaprekar(int[] num) </h4><br>
	 * Here Kaprekar constant is calculated 
	 * according to the algorithm given
	 * @param num
	 */	
	public static void kaprekar(int[] num){//Here we get Kaprekar's constant
		if (isValid(num)){
			int[] num1 = new int[4], num2 = new int[4], kap = {6, 1, 7, 4};
			int i=0;
			while (num!=kap){//If number is not 6174

				System.out.println("\nNumber: "); printNum(num); //Prints num

				num1=numLower(num); //Descending order (>)
				System.out.println("Descending order: "); printNum(num1); //Prints num1

				num2=numUpper(num); //Ascending order (<)
				System.out.println("Ascending order: "); printNum(num2); //Prints num2

				num=subtraction(num1, num2); //Subtracts
				System.out.println("Subtraction: "); printNum(num); //Prints result

				i++; //Iterations counter
			}
			System.out.println("\nIt has been reached in the constant Kaprekar"+i+
					" iterations");
		}
	}
	
	/**
	 * <h4>inputNum(int n) </h4><br>
	 * Here Kaprekar constant is calculated 
	 * according to the algorithm given
	 * @param n number of numbers to introduce
	 * @return array of numbers 
	 */	
	public int[][] inputNum(int n){
		int [][] nums = new int[n][4];
		String inputNum =  "", s;
		Scanner sc = new Scanner(System.in);
		for (int i=0; i<n; i++){
			while (inputNum==null || inputNum.length()!=4){ 
				while (!(isNumber(inputNum)&&inputNum.length()!=5)){
					inputNum=sc.nextLine();
				}
				for (int j=0; i<inputNum.length(); j++){
					s=""+inputNum.charAt(j);
					nums[i][j]=Integer.parseInt(s);
				}
			}
		}
		sc.close();
		return nums;
	}
	

}
